from flask import Flask, request, jsonify, make_response
# from model import Employee, Person, Address, db
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
import re
import uuid
import jwt
from datetime import datetime, timedelta
from functools import wraps

app = Flask(__name__)
ma = Marshmallow(app)
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = '48097e31-d0d0-41b4-a84c-71592d0408c5'

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:rehan1310@localhost/rest_employee_db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(128), unique=True)
    position = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    admin = db.Column(db.Boolean)
    persons = db.relationship('Person', backref=db.backref('employee', lazy='joined'), lazy='select')
    addresses = db.relationship('Address', backref=db.backref('employee', lazy='joined'), lazy='select')


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    branch = db.Column(db.String(128), nullable=False)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)


# db.create_all()

class EmployeeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Employee
        include_relationships = True
        # fields = ('id', 'public_id', 'position', 'email', 'admin')


employeelist_schema = EmployeeSchema(many=False)
employeelist_all_schema = EmployeeSchema(many=True)


class PersonSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Person
        include_fk = True
        # fields = ('id', 'name', 'employee_id')


personlist_schema = PersonSchema(many=False)
personlist_all_schema = PersonSchema(many=True)


class AddressSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Address
        include_fk = True
        # fields = ('id', 'branch', 'employee_id')


addresslist_schema = AddressSchema(many=False)
addresslist_all_schema = AddressSchema(many=True)


# function for verifying the JWT
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'})

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], options={"verify_signature": False})
            current_user = Employee.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'Token is invalid'}), 401

        return f(current_user, *args, **kwargs)

    return decorated


@app.route('/employee', methods=['GET'])
@token_required
def get_all_employees(current_user):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})
    employee = Employee.query.all()
    return employeelist_all_schema.jsonify(employee)


@app.route('/employee/<email>', methods=['GET'])
@token_required
def get_one_employee(current_user, email):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(email=email).first()
    return employeelist_schema.jsonify(employee)


@app.route('/employee', methods=['POST'])
@token_required
def create_new_employee(current_user):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    data = request.get_json()
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    email = data['email']
    if not (re.fullmatch(regex, email)):
        return make_response(jsonify({'Message': 'Invalid Email'}))

    new_employee = Employee(public_id=str(uuid.uuid4()), position=data['position'], email=email, admin=False)
    # new_person = Person(name=data['name'], employee_id=new_employee.id)
    # new_addr = Address(branch=data['branch'], employee_id=new_employee.id)

    db.session.add(new_employee)
    # db.session.add(new_person)
    # db.session.add(new_addr)
    db.session.commit()
    return employeelist_schema.jsonify(new_employee)


@app.route('/employee/<public_id>', methods=['PUT'])
@token_required
def update_employee(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    data = request.get_json()

    if not data:
        return make_response(jsonify({'message': "Please Input value to update"}), 401)
    if not employee:
        return make_response(jsonify({'message': "Invalid Id"}), 404)
    if 'position' in data:
        employee.position = data['position']
    if 'email' in data:
        employee.email = data['email']
    if 'admin' in data:
        employee.admin = data['admin']

    db.session.commit()
    return employeelist_schema.jsonify(employee)


@app.route('/employee/<public_id>', methods=['DELETE'])
@token_required
def delete_employee(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Detail not available"}), 404)
    db.session.delete(employee)
    db.session.commit()
    return make_response(jsonify({"message": "Detail deleted"}))


@app.route('/token', methods=['GET', 'POST'])
def get_token():
    auth = request.form
    email = auth.get('email')

    employee = Employee.query.filter_by(email=email).first()

    if employee:
        token = jwt.encode({'public_id': employee.public_id,
                            'exp': datetime.utcnow() + timedelta(minutes=15)},
                           app.config['SECRET_KEY'])
        return jsonify({'token': token})

    return make_response('could not verify', 401, {'WWW-Authenticate': 'Basic realm = "Login required!"'})

@app.route('/person', methods=['GET'])
@token_required
def get_all_person(current_user):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    person = Person.query.all()
    return personlist_all_schema.jsonify(person)

@app.route('/person/<public_id>', methods=['GET'])
@token_required
def get_one_person(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    person = Person.query.filter_by(employee_id=employee.id).first()
    return personlist_schema.jsonify(person)


@app.route('/person/<public_id>', methods=['POST'])
@token_required
def create_person(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    data = request.get_json()
    employee = Employee.query.filter_by(public_id=public_id).first()
    new = Person(name=data['name'], employee_id=employee.id)
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    db.session.add(new)
    db.session.commit()
    return personlist_schema.jsonify(new)

@app.route('/person/<public_id>', methods=['PUT'])
@token_required
def update_person(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    person = Person.query.filter_by(employee_id=employee.id).first()
    data = request.get_json()
    person.name = data['name']
    db.session.commit()

    return personlist_schema.jsonify(person)


@app.route('/person/<public_id>', methods=['DELETE'])
@token_required
def delete_person(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    person = Person.query.filter_by(employee_id=employee.id).first()
    db.session.delete(person)
    db.session.commit()
    return make_response(jsonify({"message": "Detail deleted"}))


@app.route('/address', methods=['GET'])
@token_required
def get_all_adress(current_user):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    address = Address.query.all()
    return addresslist_all_schema.jsonify(address)


@app.route('/address/<public_id>', methods=['GET'])
@token_required
def get_one_address(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    address = Address.query.filter_by(employee_id=employee.id).first()
    return addresslist_schema.jsonify(address)


@app.route('/address/<public_id>', methods=['POST'])
@token_required
def create_address(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    data = request.get_json()
    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))

    new = Address(branch=data['branch'], employee_id=employee.id)
    db.session.add(new)
    db.session.commit()
    return addresslist_schema.jsonify(new)


@app.route('/address/<public_id>', methods=['PUT'])
@token_required
def update_address(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    address = Address.query.filter_by(employee_id=employee.id).first()
    data = request.get_json()
    address.branch = data['branch']
    db.session.commit()

    return addresslist_schema.jsonify(address)


@app.route('/address/<public_id>', methods=['DELETE'])
@token_required
def delete_address(current_user, public_id):
    if not current_user.admin:
        return jsonify({"message": "The user has no authentication"})

    employee = Employee.query.filter_by(public_id=public_id).first()
    if not employee:
        return make_response(jsonify({"message": "Invalid Public id"}))
    address = Address.query.filter_by(employee_id=employee.id).first()
    db.session.delete(address)
    db.session.commit()
    return make_response(jsonify({"message": "Detail deleted"}))

if __name__ == '__main__':
    app.run(debug=True)
