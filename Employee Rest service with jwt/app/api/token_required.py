from functools import wraps
from flask import request, jsonify
import jwt
from .main import app
from .models import Employee
import logging

# app = create_app()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

f = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh = logging.FileHandler('token_required.log')
fh.setFormatter(f)

logger.addHandler(fh)


# function for verifying the JWT
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            logger.info('Token is missing')
            return jsonify({'message': 'Token is missing!'})

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], options={"verify_signature": False})
            current_user = Employee.query.filter_by(public_id=data['public_id']).first()
        except:
            logger.warning('Token is invalid')
            return jsonify({'message': 'Token is invalid'}), 401

        return f(current_user, *args, **kwargs)

    return decorated
