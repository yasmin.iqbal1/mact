# launches the Jaeger UI, collector, query, and agent
# sudo systemctl status docker
# sudo docker run -d -p6831:6831/udp -p16686:16686 jaegertracing/all-in-one:latest
# http://127.0.0.1:16686/

import logging
from jaeger_client import Config


def initialize_tracer(service):
    logging.getLogger('').handlers = []
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    config = Config(
        config={ # usually read from some yaml config
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            'logging': True,
            'reporter_batch_size': 1,
        },
        service_name=service,
    )

    # this call also sets opentracing.tracer
    return config.initialize_tracer()



