from .main import app, mongo
from flask import jsonify, request
from bson.objectid import ObjectId
from bson.json_util import dumps
# from .mongo_models import Employees
# from .models import Employee
# from .serializers import employeelist_all_schema

# app = create_app()

# @app.route('/')
# def test():
#     employee = Employee.query.all()
#     output = []
#     for i in employee:
#         output.append({'admin': i.admin,
#                        'position': i.position,
#                        'email': i.email})
#     print(output)
#     resp = mongo.db.employee.insert(output)
#     return jsonify({'output': resp})


@app.route('/emp', methods=['GET'])
def get_all():
    # employees = Employees.object.all()
    employees = mongo.db.employee.find()
    resp = dumps(employees)
    return resp



# @app.route('/emp', methods=['POST'])
# def create_employee():
#     _json = request.json
#     _name = _json.get('name')
#     _admin = 'false'
#     _branch = _json.get('branch')
#     _email = _json.get('email')
#     _position = _json.get('position')
#     # employee = Employee.query.all()
#     # output = []
#     # for i in employee:
#     #     id = mongo.db.employee.insert({'admin': i.admin,
#     #                        'position': i.position,
#     #                        'email': i.email})
#     #     output.append({id})
#     id = mongo.db.employee.insert({
#         'name': _name, 'admin': _admin, 'branch': _branch,
#         'email': _email, 'position': _position
#     })
#     resp = jsonify("Employee added successfully")
#
#     resp.status_code = 200
#     return resp


