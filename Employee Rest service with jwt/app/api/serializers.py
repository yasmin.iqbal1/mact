from .schemas import ma
from .models import Employee, Person, Address


class EmployeeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Employee
        include_relationships = True
        # fields = ('id', 'public_id', 'position', 'email', 'admin')


employeelist_schema = EmployeeSchema(many=False)
employeelist_all_schema = EmployeeSchema(many=True)


class PersonSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Person
        include_fk = True
        # fields = ('id', 'name', 'employee_id')


personlist_schema = PersonSchema(many=False)
personlist_all_schema = PersonSchema(many=True)


class AddressSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Address
        include_fk = True
        # fields = ('id', 'branch', 'employee_id')


addresslist_schema = AddressSchema(many=False)
addresslist_all_schema = AddressSchema(many=True)
