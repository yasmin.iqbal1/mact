import sys

from flask import Flask
from api.database import db
from api.schemas import ma
import logging
from api import responses as resp
from api.responses import response_with
from flask_swagger_ui import get_swaggerui_blueprint
from .mongo_extensions import mongo


# def create_app():
app = Flask(__name__)

# if app.config['ENV'] == "production":
#     app.config.from_object('config.ProductionConfig')
# elif app.config['ENV'] == "testing":
#     app.config.from_object('config.TestingConfig')
# else:
#     app.config.from_object('config.DevelopmentConfig')
app.config.from_object('config.DevelopmentConfig')

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

f = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh = logging.FileHandler('main.log')
fh.setFormatter(f)

logger.addHandler(fh)

@app.after_request
def add_header(response):
    return response

@app.errorhandler(400)
def bad_request(e):
    logger.error(e)
    return response_with(resp.BAD_REQUEST_400)

@app.errorhandler(500)
def server_error(e):
    logger.error(e)
    return response_with(resp.SERVER_ERROR_500)

@app.errorhandler(404)
def not_found(e):
    logger.error(e)
    return response_with(resp.SERVER_ERROR_404)

# swagger configs
SWAGGER_URL = '/swagger'
API_URI = '/static/spec.yml'
SWAGGER_BLUEPRINT = get_swaggerui_blueprint(SWAGGER_URL,
                                            API_URI,
                                            config={'app name': "Employee Rest Api"}
                                            )
app.register_blueprint(SWAGGER_BLUEPRINT, url_prefix=SWAGGER_URL)


db.init_app(app)
with app.app_context():
    db.create_all()

ma.init_app(app)

mongo.init_app(app)
    # return app


from api import views
from api import get_token
from api import token_required
from api import mongo_views

