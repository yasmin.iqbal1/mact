from .database import db


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(128), unique=True)
    position = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    admin = db.Column(db.Boolean)
    persons = db.relationship('Person', backref=db.backref('employee', lazy='joined'),
                              lazy='select', cascade="all, delete-orphan")
    addresses = db.relationship('Address', backref=db.backref('employee', lazy='joined'),
                                lazy='select', cascade="all, delete-orphan")


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    branch = db.Column(db.String(128), nullable=False)
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)
