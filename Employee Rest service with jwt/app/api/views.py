import logging
import opentracing.tracer
from .main import app
from .models import Employee, Person, Address
from .serializers import employeelist_schema, employeelist_all_schema, personlist_schema, personlist_all_schema, \
    addresslist_schema, addresslist_all_schema
from flask import request, make_response, jsonify
from .token_required import token_required
import uuid
import re
from .database import db
from .responses import response_with
from api import responses as resp
from flask_opentracing import FlaskTracer
from .tracing import initialize_tracer


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

f = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

fh = logging.FileHandler('views.log')
fh.setFormatter(f)

logger.addHandler(fh)


@app.route('/employee', methods=['GET'])
@token_required
def get_all_employees(current_user):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)

    with opentracing.tracer.start_span('get-employee', child_of=span) as span1:
        span1.set_tag('method', 'GET')
        employee = Employee.query.all()
        logger.info('Fetched all employees')
        span1.set_tag("http.status_code", resp.SUCCESS_200)
        return employeelist_all_schema.jsonify(employee)


@app.route('/employee/<email>', methods=['GET'])
@token_required
def get_one_employee(current_user, email):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
        try:
            with opentracing.tracer.start_span('get-single-employee', child_of=span) as span1:
                span1.set_tag('method', 'GET')
                employee = Employee.query.filter_by(email=email).first()
                logger.info('Fetched single employee')
                span1.set_tag("http.status_code", resp.SUCCESS_200)
                return employeelist_schema.jsonify(employee)
        except Exception as e:
            with opentracing.tracer.start_span('error', child_of=span1) as span2:
                span2.set_tag("http.status_code", resp.INVALID_INPUT_422)
                logger.debug(e)
                return response_with(resp.INVALID_INPUT_422)


@app.route('/employee', methods=['POST'])
@token_required
def create_new_employee(current_user):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('create-employee', child_of=span) as span1:
            data = request.get_json()
            regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
            email = data['email']
            span1.set_tag('email', 'isvalid')
            if not (re.fullmatch(regex, email)):
                span1.set_tag('status', resp.INVALID_INPUT_422)
                return response_with(resp.INVALID_INPUT_422)
            with opentracing.tracer.start_span('create-detail', child_of=span1) as span2:
                span2.set_tag('method', 'post')
                new_employee = Employee(public_id=str(uuid.uuid4()), position=data['position'], email=email, admin=False)

                db.session.add(new_employee)
                db.session.commit()
                logger.info('New employee created')
                span2.set_tag("http.status_code", resp.SUCCESS_200)
                return employeelist_schema.jsonify(new_employee)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span2) as span3:
            span3.set_tag("http.status_code", resp.INVALID_INPUT_422)
            logger.debug(e)
            return response_with(resp.INVALID_INPUT_422)


@app.route('/employee/<public_id>', methods=['PUT'])
@token_required
def update_employee(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
        try:
            with opentracing.tracer.start_span('update', child_of=span) as span1:
                span1.set_tag('method', 'put')
                employee = Employee.query.filter_by(public_id=public_id).first()
                data = request.get_json()
                if not employee:
                    span1.set_tag("http.status_code", resp.INVALID_INPUT_422)
                    return response_with(resp.INVALID_INPUT_422)
            with opentracing.tracer.start_span('detail', child_of=span1) as span2:
                span2.set_tag('detail', 'update')
                if 'position' in data:
                    employee.position = data['position']
                if 'email' in data:
                    employee.email = data['email']
                if 'admin' in data:
                    employee.admin = data['admin']

                db.session.commit()
                logger.info('Employee updated')
                span2.set_tag("http.status_code", resp.SUCCESS_200)
                return employeelist_schema.jsonify(employee)
        except Exception as e:
            with opentracing.tracer.start_span('error', child_of=span2) as span3:
                span3.set_tag("http.status_code", resp.MISSING_PARAMETERS_422)
                logger.debug(e)
                return response_with(resp.MISSING_PARAMETERS_422)


@app.route('/employee/<public_id>', methods=['DELETE'])
@token_required
def delete_employee(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('update', child_of=span) as span1:
            span1.set_tag('method', 'delete')
            employee = Employee.query.filter_by(public_id=public_id).first()
            db.session.delete(employee)
            db.session.commit()
            logger.info('Employee detail deleted')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return response_with(resp.SUCCESS_200, value={'message': "Detail deleted"})
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.SERVER_ERROR_404)
            logger.debug(e)
            return response_with(resp.SERVER_ERROR_404)


@app.route('/person', methods=['GET'])
@token_required
def get_all_person(current_user):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    with opentracing.tracer.start_span('get-person', child_of=span) as span1:
        span1.set_tag('method', 'GET')
        person = Person.query.all()
        logger.info('Fetched all person')
        span1.set_tag('http.status_code',resp.SUCCESS_200)
    return personlist_all_schema.jsonify(person)


@app.route('/person/<public_id>', methods=['GET'])
@token_required
def get_one_person(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('person-detail', child_of=parent_span) as span1:
            span1.set_tag('method', 'GET')
            employee = Employee.query.filter_by(public_id=public_id).first()
            person = Person.query.filter_by(employee_id=employee.id).first()
            logger.info('Fetched single person')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return personlist_schema.jsonify(person)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.INVALID_INPUT_422)
            logger.debug(e)
            return response_with(resp.INVALID_INPUT_422)


@app.route('/person/<public_id>', methods=['POST'])
@token_required
def create_person(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('create-detail', child_of=parent_span) as span1:
            span1.set_tag('method', 'POST')
            data = request.get_json()
            employee = Employee.query.filter_by(public_id=public_id).first()
            new = Person(name=data['name'], employee_id=employee.id)
            db.session.add(new)
            db.session.commit()
            logger.info('New person created')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return personlist_schema.jsonify(new)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.INVALID_INPUT_422)
            logger.debug(e)
            return response_with(resp.INVALID_INPUT_422)


@app.route('/person/<public_id>', methods=['PUT'])
@token_required
def update_person(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('update-detail', child_of=span) as span1:
            span1.set_tag('method', 'PUT')
            employee = Employee.query.filter_by(public_id=public_id).first()
            if not employee:
                span1.set_tag('http.status_code', resp.INVALID_INPUT_422)
                logger.warning(resp.INVALID_INPUT_422)
                return response_with(resp.INVALID_INPUT_422)
            with opentracing.tracer.start_span('update-detail', child_of=span1) as span2:
                person = Person.query.filter_by(employee_id=employee.id).first()
                data = request.get_json()
                person.name = data['name']
                db.session.commit()
                span2.set_tag('http.status_code', resp.SUCCESS_200)
                logger.info('Person detail updated')
                return personlist_schema.jsonify(person)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span2) as span3:
            span3.set_tag("http.status_code", resp.INVALID_INPUT_422)
            logger.debug(e)
            return response_with(resp.MISSING_PARAMETERS_422)


@app.route('/person/<public_id>', methods=['DELETE'])
@token_required
def delete_person(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('DELETE', child_of=span) as span1:
            span1.set_tag('method', 'delete')
            employee = Employee.query.filter_by(public_id=public_id).first()
            person = Person.query.filter_by(employee_id=employee.id).first()
            db.session.delete(person)
            db.session.commit()
            logger.info('Person detail deleted')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return response_with(resp.SUCCESS_200, value={'message': "Detail deleted"})
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.SERVER_ERROR_404)
            logger.debug(e)
            return response_with(resp.SERVER_ERROR_404)


@app.route('/address', methods=['GET'])
@token_required
def get_all_address(current_user):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    with opentracing.tracer.start_span('get-address', child_of=span) as span1:
        span1.set_tag('method', 'GET')
        address = Address.query.all()
        span1.set_tag('http.status_code', resp.SUCCESS_200)
        logger.info('Fetched all address')
        return addresslist_all_schema.jsonify(address)


@app.route('/address/<public_id>', methods=['GET'])
@token_required
def get_one_address(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('address-detail', child_of=parent_span) as span1:
            span1.set_tag('method', 'GET')
            employee = Employee.query.filter_by(public_id=public_id).first()
            address = Address.query.filter_by(employee_id=employee.id).first()
            logger.info('Fetched single address')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return addresslist_schema.jsonify(address)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.INVALID_INPUT_422)
            logger.debug(e)
            return response_with(resp.INVALID_INPUT_422)


@app.route('/address/<public_id>', methods=['POST'])
@token_required
def create_address(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('address', child_of=parent_span) as span1:
            span1.set_tag('method', 'POST')
            data = request.get_json()
            employee = Employee.query.filter_by(public_id=public_id).first()
            new = Address(branch=data['branch'], employee_id=employee.id)
            db.session.add(new)
            db.session.commit()
            logger.info('New Address created')
            span1.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return addresslist_schema.jsonify(new)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.INVALID_INPUT_422)
            logger.debug(e)
            return response_with(resp.INVALID_INPUT_422)


@app.route('/address/<public_id>', methods=['PUT'])
@token_required
def update_address(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('address-update', child_of=parent_span) as span1:
            span1.set_tag('method', 'PUT')
            employee = Employee.query.filter_by(public_id=public_id).first()

            if not employee:
                logger.warning(resp.INVALID_INPUT_422)
                return response_with(resp.INVALID_INPUT_422)

            address = Address.query.filter_by(employee_id=employee.id).first()
            data = request.get_json()
            address.branch = data['branch']
            db.session.commit()
            logger.info('Address detail updated')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return addresslist_schema.jsonify(address)
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.MISSING_PARAMETERS_422)
            logger.debug(e)
            return response_with(resp.MISSING_PARAMETERS_422)


@app.route('/address/<public_id>', methods=['DELETE'])
@token_required
def delete_address(current_user, public_id):
    parent_span = tracer.get_span()
    with opentracing.tracer.start_span('check-admin', child_of=parent_span) as span:
        span.set_tag('admin-status', 'status')
        if not current_user.admin:
            logger.warning('User has no authentication')
            span.set_tag('http.status_code', resp.UNAUTHORIZED_403)
            return response_with(resp.UNAUTHORIZED_403)
    try:
        with opentracing.tracer.start_span('DELETE', child_of=span) as span1:
            span1.set_tag('method', 'delete')
            employee = Employee.query.filter_by(public_id=public_id).first()
            address = Address.query.filter_by(employee_id=employee.id).first()
            db.session.delete(address)
            db.session.commit()
            logger.info('Address detail deleted')
            span1.set_tag('http.status_code', resp.SUCCESS_200)
            return response_with(resp.SUCCESS_200, value={'message': "Detail deleted"})
    except Exception as e:
        with opentracing.tracer.start_span('error', child_of=span1) as span2:
            span2.set_tag("http.status_code", resp.SERVER_ERROR_404)
            logger.debug(e)
            return response_with(resp.SERVER_ERROR_404)


tracer = FlaskTracer(initialize_tracer('employee service'), trace_all_requests=True, app=app)


# Pagination
@app.route('/employee/page', methods=['GET'])
def get_employees_pagination():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 5, type=int)
    employee = Employee.query.paginate(page=page, per_page=per_page)
    data = []
    for emp in employee.items:
        data.append({
            'id': emp.id,
            'public_id': emp.public_id,
            'position': emp.position,
            'email': emp.email,
            'admin': emp.admin
        })
        meta = {
            'page': employee.page,
            'pages': employee.pages,
            'total_count': employee.total,
            'prev_page': employee.prev_num,
            'next_page': employee.next_num,
            'has_next': employee.has_next,
            'has_prev': employee.has_prev
        }
    return jsonify({'data': data}, {'meta': meta})


@app.route('/person/page', methods=['GET'])
def get_person_pagination():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 5, type=int)
    person = Person.query.paginate(page=page, per_page=per_page)
    data = []
    for pers in person.items:
        data.append({
            'id': pers.id,
            'name': pers.name,
            'employee_id': pers.employee_id
        })
        meta = {
            'page': person.page,
            'pages': person.pages,
            'total_count': person.total,
            'prev_page': person.prev_num,
            'next_page': person.next_num,
            'has_next': person.has_next,
            'has_prev': person.has_prev
        }
    return jsonify({'data': data}, {'meta': meta})


@app.route('/address/page', methods=['GET'])
def get_address_pagination():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 5, type=int)
    address = Address.query.paginate(page=page, per_page=per_page)
    data = []
    for addr in address.items:
        data.append({
            'id': addr.id,
            'branch': addr.branch,
            'employee_id': addr.employee_id
        })
        meta = {
            'page': address.page,
            'pages': address.pages,
            'total_count': address.total,
            'prev_page': address.prev_num,
            'next_page': address.next_num,
            'has_next': address.has_next,
            'has_prev': address.has_prev
        }
    return jsonify({'data': data}, {'meta': meta})
