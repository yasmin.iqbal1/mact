from .main import app
from flask import request, jsonify, make_response
from .models import Employee
from datetime import datetime, timedelta
import jwt
import logging
from .tracing import initialize_tracer
from flask_opentracing import FlaskTracer

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

f = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh = logging.FileHandler('get_token.log')
fh.setFormatter(f)

logger.addHandler(fh)

# app = create_app()


@app.route('/token', methods=['GET', 'POST'])
def get_token():
    auth = request.args
    email = auth.get('email')

    employee = Employee.query.filter_by(email=email).first()

    if employee:
        token = jwt.encode({'public_id': employee.public_id,
                            'exp': datetime.utcnow() + timedelta(minutes=15)},
                           app.config['SECRET_KEY'])
        return jsonify({'token': token})
    logger.warning('could not verify')
    return make_response('could not verify', 401, {'WWW-Authenticate': 'Basic realm = "Login required!"'})


tracer = FlaskTracer(initialize_tracer('employee service'), True, app)
