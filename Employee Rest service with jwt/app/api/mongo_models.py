from .mongo_extensions import mongo


class Employees(mongo.db.Document):
    name = mongo.db.String()
    admin = mongo.db.Boolean()
    branch = mongo.db.String()
    email = mongo.db.String()
    position = mongo.db.String()
