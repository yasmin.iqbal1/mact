class Config(object):
    DEBUG = False
    TESTING = False

    SECRET_KEY = '48097e31-d0d0-41b4-a84c-71592d0408c5'

    DB_NAME = 'production-db'
    DB_USERNAME = 'root'
    DB_PASSWORD = 'rehan1310'

    SESSION_COOKIE_SECURE = True

    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    pass


class DevelopmentConfig(Config):
    DEBUG = True
    DB_NAME = 'rest_employee_db'
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqlconnector://root:rehan1310@localhost/rest_employee_db'
    MONGO_URI = 'mongodb+srv://yasmin-iqbal:rehan1310@cluster0.heojx.mongodb.net/restapi_db?retryWrites=true&w=majority'
    # MONGO_URI = "mongodb://localhost:27017/restapi_db"
    SESSION_COOKIE_SECURE = False


class TestingConfig(Config):
    TESTING = True
    DB_NAME = 'rest_employee_db'
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqlconnector://root:rehan1310@localhost/rest_employee_db'
    SESSION_COOKIE_SECURE = False
