from django.contrib import admin
from .models import Employees
from .models import Person
from .models import Address
from django.contrib.auth.models import User


admin.site.register(Employees)
admin.site.register(Person)
admin.site.register(Address)
