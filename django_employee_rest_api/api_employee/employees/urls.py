from . import views
from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()
router.register('employees', views.EmployeeList)
router.register('persons', views.PersonList)
router.register('address', views.AddressList)


urlpatterns = [
    path('', include(router.urls)),
    path('users/', views.UserCreate.as_view())
]
