from django.db import models
from uuid import uuid4


class Employees(models.Model):
    public_id = models.UUIDField(default=str(uuid4()), unique=True, editable=False)
    position = models.CharField(max_length=200)
    email = models.EmailField('Email Address', unique=True, null=False)
    admin = models.BooleanField(default=False)

    def __str__(self):
        return self.position + ' ' + self.email


class Person(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.ForeignKey(Employees, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


branch_choice = (
    ('CHN', 'Chennai'),
    ('MDU', 'Madurai'),
    ('TR', 'Trichy')
)


class Address(models.Model):
    branch = models.CharField(max_length=20, choices=branch_choice, default=None)
    employee_id = models.ForeignKey(Employees, on_delete=models.CASCADE)

    def __str__(self):
        return self.branch