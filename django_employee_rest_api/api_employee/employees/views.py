from django.shortcuts import render
from rest_framework import viewsets, generics
from .models import Employees, Person, Address
from .serializers import EmployeeSerializer, AddressSerializer, PersonSerializer, UserSerializer
from rest_framework.permissions import IsAuthenticated


class EmployeeList(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated, )
    queryset = Employees.objects.all()
    serializer_class = EmployeeSerializer
    # permission_classes = permissions.IsAuthenticatedOrReadOnly


class PersonList(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class AddressList(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer


class UserCreate(generics.CreateAPIView):
    serializer_class = UserSerializer
